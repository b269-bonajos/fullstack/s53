import{useState, useContext} from 'react';

import {Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AppNavbar() {

  // to store user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem('email'));

  const {user} = useContext(UserContext);

  return (
    <Navbar bg="light" expand="lg">
        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

            { (user.email !== null) ?
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            :
            <>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            </>

            }

          </Nav>
        </Navbar.Collapse>
    </Navbar>
  );
}
