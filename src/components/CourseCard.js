import {useState, useEffect} from 'react';

// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard({course}) {

  // Deconstruct the course properties into their own variables
  const { name, description, price } = course; 

  // STYNTAX:
  // const [getter, setter] = useState (initialGetterValue);
  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(5);
  

 function enroll() {
    // if (seats > 0) {
    //   setCount(count + 1);
    //   console.log('Enrollees: ' + count)
    //   setSeats(seats - 1)
    //   console.log('Seats: ' + seats);
    // } else {
    //   alert("No more seats available");
    // };
    setCount(count + 1);
    setSeats(seats - 1);
  }

useEffect(() => {
    if (seats <= 0){
        alert("No more seats avalable!")
    }
}, [seats]);

return (
    
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                        <Card.Subtitle>Count: {count} </Card.Subtitle>
                        <Card.Subtitle>Seats: {seats} </Card.Subtitle>
                        <Button variant="primary" onClick={enroll}disabled={seats<=0}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
// [S50 ACTIVITY END]


