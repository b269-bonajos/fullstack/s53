import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses() {

	console.log(coursesData);

	const courses = coursesData.map(course => {
		return (
			< CourseCard key={course.id} course = {course} />
			)
	})

	return (
		<>
		{courses}
		</>
	)
}

/*Way to declare props drilliing*/
/*We can ppass information from one component to another using props(props drilling*/
/*Curly braces {} are used for props to signify that we are providing/passing information from one component to another using JS expression*/